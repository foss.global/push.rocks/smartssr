import { expect, tap } from '@pushrocks/tapbundle';
import * as smartssr from '../ts/index.js';

let testSSRInstance: smartssr.SmartSSR;

tap.test('should create a valid smartssr instance', async () => {
  testSSRInstance = new smartssr.SmartSSR({
    debug: true,
  });
});

tap.test('should render central.eu', async (tools) => {
  await testSSRInstance.renderPage('https://lossless.com');
});

tap.test('should render lossless.com', async () => {
  await testSSRInstance.renderPage('https://lossless.com');
});

tap.test('should render https://lossless.gmbh', async () => {
  const renderedPage = await testSSRInstance.renderPage('https://lossless.gmbh');
  console.log(renderedPage);
});

tap.start();
