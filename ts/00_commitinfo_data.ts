/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartssr',
  version: '1.0.37',
  description: 'a smart server side renderer supporting shadow dom'
}
