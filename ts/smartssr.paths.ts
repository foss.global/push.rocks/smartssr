import * as plugins from './smartssr.plugins.js';

export const packageDir = plugins.path.join(plugins.smartpath.get.dirnameFromImportMetaUrl(import.meta.url), '../');
export const noGitDir = plugins.path.join(packageDir, './.nogit');
plugins.smartfile.fs.ensureDirSync(noGitDir);
